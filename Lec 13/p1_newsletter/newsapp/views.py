from django.shortcuts import render
from .models import StudentModel
from .forms import StudentForm

def home(request):
    if request.method =="POST" and request.POST.get("b1"):
        em = request.POST.get("email")
        try:
            usr = StudentModel.objects.get(email=em)
            fm  = StudentForm()
            return render(request,"home.html",{"fm":fm,"msg":"Email already Registered"})
        except StudentModel.DoesNotExist:
            data = StudentForm(request.POST)
            if data.is_valid:

                data.save()
            fm  = StudentForm()
            return render(request,"home.html",{"fm":fm,"msg":"Email Registered ThankYou!"})
    
    elif request.method =="POST" and request.POST.get("b2"):
        em = request.POST.get("email")
        try:
            usr = StudentModel.objects.get(email=em)
            fm = StudentForm()
            usr.delete()
            return render(request,"home.html",{"fm":fm,"msg":"Unsubscribed"})
        except StudentModel.DoesNotExist:
            fm = StudentForm()
            return render(request,"home.html",{"fm":fm,"msg":"User Not Found"})
    else:        
        fm = StudentForm()
        return render(request,"home.html",{"fm":fm,"msg":"msg"})

