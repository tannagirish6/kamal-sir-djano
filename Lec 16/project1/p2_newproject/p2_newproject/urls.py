from django.contrib import admin
from django.urls import path
from newdtapp.views import dt,ti,dtti
urlpatterns = [
    path('admin/', admin.site.urls),
    path("dt",dt),
    path("ti",ti),
    path("dtti",dtti),
]
