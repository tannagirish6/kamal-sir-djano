from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from datetime import *

@api_view(["GET"])
def dt(request):
    d = datetime.now().date()
    msg = "sever date = " +str(d)
    return Response({"msg":msg})

@api_view(["GET"])
def ti(request):
    d = datetime.now().time()
    msg = "sever time = " +str(d)
    return Response({"msg":msg})

@api_view(["GET"])
def dtti(request):
    d = datetime.now()
    msg = "sever date = " +str(d)
    return Response({"msg":msg})

