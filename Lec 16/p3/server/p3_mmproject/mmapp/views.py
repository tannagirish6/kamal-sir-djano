from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from random import *
# Create your views here.
mm = [  "Believe you can and you're halfway there. -Theodore Roosevelt",  "The only way to do great work is to love what you do. -Steve Jobs",  "You are never too old to set another goal or to dream a new dream. -C.S. Lewis",  "Success is not final, failure is not fatal: it is the courage to continue that counts. -Winston Churchill",  "Believe in yourself and all that you are. Know that there is something inside you that is greater than any obstacle. -Christian D. Larson",  "If you can dream it, you can achieve it. -Zig Ziglar",  "You miss 100% of the shots you don't take. -Wayne Gretzky",  "I have not failed. I've just found 10,000 ways that won't work. -Thomas Edison",  "The only limit to our realization of tomorrow will be our doubts of today. -Franklin D. Roosevelt",  "Don't watch the clock; do what it does. Keep going. -Sam Levenson"]
@api_view(["GET"])
def home(request):
    r = randint(0,len(mm)-1)
    msg = mm[r]
    return Response({"msg":msg})
