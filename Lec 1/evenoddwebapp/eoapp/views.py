from django.shortcuts import render

# Create your views here.
def home(req):
    if req.GET.get("num"):
        num = int(req.GET.get("num"))
        if num % 2 == 0:
            msg = "even"
        else:
            msg = "odd"
        return render(req,"home.html",{"msg":msg})
    return render(req,"home.html")

    