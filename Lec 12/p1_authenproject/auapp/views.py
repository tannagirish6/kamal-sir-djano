from django.shortcuts import render,redirect
from django.contrib.auth import User
from django.contrib.auth import login,logout,authenticate
# Create your views here.
def uhome(request):
    if request.user.is_authenticated:
        return redirect("uwelcome")
    else:
        if request.method == "POST":
            un  = request.POST.get("un")
            pw  = request.POST.get("pw")
            usr = authenticate(username=un,password=pw)
            if usr is None:
                return render(request,"uhome.html",{"msg":"check username/password"})
            else:
                login(request,usr)
                return redirect("uwelcome")
        else:
            return redirect(request,"uhome.html")
        
    
def usignup(request):
    if request.user.is_authenticated:
        return redirect("uwelcome")
    else:
        if request.method == "POST":
            un =  request.POST.get("un")
            try:
                usr = User.objects.get(username=un)
                return render(request,"usignup.html",{"msg":"email already in use"})
            except User.DoesNotExist:
                usr = User.objects.create_user(username=un,password="1234")
                usr.save()
                return redirect("uhome")
        else:
            return render(request,"usignup.html")
                
def uwelcome(request):
    if request.user.is_authenticated:
        return render(request,"uwelcome.html")
    else:
        return redirect("uhome")
    
def ulogout(request):
    logout(request)
    return redirect("uhome")