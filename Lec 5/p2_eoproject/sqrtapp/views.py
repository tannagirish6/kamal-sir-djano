from django.shortcuts import render
from .forms import SqrtForm
# Create your views here.
def home(req):
    if req.GET.get("num"):
        num = float(req.GET.get("num"))
        num = num **0.5
        num = round(num,2)
        fm = SqrtForm()
        msg = ""+str(num)
        return render(req,"home.html",{"fm":fm,"msg":msg})
    else:
        msg = "number"
        fm = SqrtForm()
        return render(req,"home.html",{"msg":msg,"fm":fm})
    return render(req,"home.html")