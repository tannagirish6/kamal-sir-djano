from django.shortcuts import render
from .forms import EvenOddForm
# Create your views here.
def home(req):
    if req.GET.get("num"):
        num = int(req.GET.get("num"))
        res = "even" if num % 2 == 0 else "odd"
        msg = "res = "+res
        fm = EvenOddForm()
        return render(req,"home.html",{"fm":fm,"msg":msg})
    else:
        fm = EvenOddForm()
        return render(req,"home.html",{"fm":fm})
