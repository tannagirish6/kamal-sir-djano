from django.shortcuts import render

# Create your views here.
def home(req):
    if req.POST.get("num"):
        num = int(req.POST.get("num"))
        num = num ** 0.5
        msg = "The root is " + str(num)
        return render(req,"home.html",{"msg":msg})
    return render(req,"home.html")