from django.shortcuts import render

# Create your views here.
def home(req):
    if req.POST.get("num"):
        num = int(req.POST.get("num"))
        if num % 2 == 0:
            msg = "even"
        else:
            msg = "odd"
        result = "Your result is "+msg
        return render(req,"home.html",{"msg":result})
    return render(req,"home.html")
