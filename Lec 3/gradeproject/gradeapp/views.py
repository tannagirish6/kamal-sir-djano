from django.shortcuts import render

# Create your views here.
def home(req):
    if req.POST.get("marks"):
        marks = int(req.POST.get("marks"))
        if marks>50:
            grade="A"
        else:
            grade="B"
        msg = "Your grade is "+grade
        return render(req,"home.html",{"msg":msg})

    return render(req,"home.html")
