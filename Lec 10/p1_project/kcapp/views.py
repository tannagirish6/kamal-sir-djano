from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,"home.html")

def ml(request):
    return render(request,"ml.html")

def django(request):
    return render(request,"django.html")

def jsmern(request):
    return render(request,"jsmern.html")

def python(request):
    return render(request,"python.html")