from django.contrib import admin
from django.urls import path
from kcapp.views import python,ml,django,jsmern,home
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",home,name="home"),
    path("python",python,name="python"),
    path("ml",ml,name="ml"),
    path("django",django,name="django"),
    path("jsmern",jsmern,name="jsmern")
]
