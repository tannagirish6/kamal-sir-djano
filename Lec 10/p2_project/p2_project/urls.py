
from django.contrib import admin
from django.urls import path
from kcapp.views import home,location
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",home,name="home"),
    path("location",location,name="location")
]
