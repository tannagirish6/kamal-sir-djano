from django.shortcuts import render
from folium import *
# Create your views here.
def home(request):
    return render(request,"home.html")

def location(request):
    loc = [19.1872,72.9739]
    f = Figure(width=500,height=500)
    thane = Map(location=loc,zoom_start=18).add_to(f)
    Marker(loc,tooltip="Kamal Classes Thane").add_to(thane)
    thane_html = thane._repr_html_()
    return render(request,"location.html",{"msg":thane_html})