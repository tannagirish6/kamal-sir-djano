from django.shortcuts import render
from math import pi,pow
# Create your views here.
def home(request):
    if request.POST.get("radius"):
        radius = float(request.POST.get("radius"))
        area = pi*pow(radius,2)
        area   = round(area,2)
        circum = 2*pi*radius
        circum = round(circum,2)
        msg = "Area is "+str(area)+" Circum is "+str(circum)
        return render(request,"home.html",{"msg":msg})
    return render(request,"home.html")