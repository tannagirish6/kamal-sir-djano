from django.shortcuts import render

# Create your views here.
def home(request):
    if request.POST.get("marks"):
        marks = float(request.POST.get("marks"))
        if marks > 80:
            grade = "A"
        elif marks >60:
            grade = "B"
        elif marks > 40:
            grade = "C"
        elif marks >=1:
            grade = "D"
        msg="Your Grade is "+grade
        return render(request,"home.html",{"grade":msg})    
    return render(request,"home.html")