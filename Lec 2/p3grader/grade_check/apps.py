from django.apps import AppConfig


class GradeCheckConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'grade_check'
