from django.apps import AppConfig


class SquareCheckConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'square_check'
