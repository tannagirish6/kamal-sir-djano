from django.shortcuts import render

# Create your views here.
def home(req):
    if req.GET.get("num"):
        num = int(req.GET.get("num"))
        sq = num*num
        msg = "Square is "+str(round(sq,2))
        return render(req,"home.html",{"msg":msg})
    return render(req,"home.html")