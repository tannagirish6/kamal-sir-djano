from django.contrib import admin
from .models import StudentModel
# Register your models here.

class EnAdmin(admin.ModelAdmin):
    list_display = ("rno","name","dt")
    list_filter  = ("rno",)
admin.site.register(StudentModel,EnAdmin)
