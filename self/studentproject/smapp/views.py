from django.shortcuts import render,redirect
from .forms import StudentForm
from .models import StudentModel
# Create your views here.
def index(request):
    data = StudentModel.objects.all()

    return render(request,"index.html",{"data":data})

def remove(request,id):
    data = StudentModel.objects.get(rno=id)
    data.delete()
    return redirect("index")

def create(request):
    if request.method == "POST":
        data = StudentForm(request.POST)
        if data.is_valid():
            data.save()  # Save the form data using the 'data' instance
            fm = StudentForm()
            msg = "success"
            
            return render(request, "create.html", {"fm": fm, "msg": msg})
        else:
            fm = StudentForm()
            msg = "Invalid data"
            return render(request, "create.html", {"fm": fm, "msg": msg})
    
    fm = StudentForm()
    return render(request, "create.html", {"fm": fm})