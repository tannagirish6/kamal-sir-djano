# Generated by Django 4.1.7 on 2023-02-26 10:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StudentModel',
            fields=[
                ('rno', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=40)),
                ('dt', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
