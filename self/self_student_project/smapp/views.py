from django.shortcuts import render,redirect
from .models import StudentModel
from .forms import StudentForm
# Create your views here.
def remove(request,id):
    st = StudentModel.objects.get(rno=id)
    st.delete()
    return redirect("home")

def home(request):
    data = StudentModel.objects.all()
    return render(request,"home.html",{"data":data})

def register(request):
    form = StudentForm()
    if request.method=="POST":
        data = StudentForm(request.POST)
        print(data)
        if data.is_valid():
            data.save()
            msg = "data saved!"
            return render(request,"register.html",{"form":form,"msg":msg})
    else:
       return render(request,"register.html",{"form":form})