from django.contrib import admin
from django.urls import path
from smapp.views import home,register,remove
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",home,name="home"),
    path("resgister",register,name="register"),
    path("remove/<int:id>",remove,name="remove")
]
