from django.db import models

# Create your models here.
class StudentModel(models.Model):
    name   = models.CharField(max_length=40)
    phone  = models.IntegerField()
    course = models.CharField(max_length=40)
    dt     = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.name)+" "+str(self.course) 