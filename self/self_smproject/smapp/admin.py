from django.contrib import admin
from .models import StudentModel
# Register your models here.

class SmAdmin(admin.ModelAdmin):
    list_display = ("name","course","dt")
    list_filter = ("dt",)

admin.site.register(StudentModel,SmAdmin)
