from django.shortcuts import render, redirect
from .forms import StudentForm

def index(request):
    return render(request,"index.html")
def create(request):
    fm = StudentForm()
    msg = ''
    if request.method == 'POST':
        fm = StudentForm(request.POST)
        if fm.is_valid():
            fm.save()
            msg = 'fm submitted successfully.'
            return render(request, 'create.html', {'fm': fm, 'msg': msg})
        else:
            msg = 'fm submission failed. Please correct the errors below.'
    else:
        fm = StudentForm()
    return render(request, 'create.html', {'fm': fm, 'msg': msg})
