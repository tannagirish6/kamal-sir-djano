from django.contrib import admin
from django.urls import path
from stuapp.views import stuop
urlpatterns = [
    path('admin/', admin.site.urls),
    path("stuop",stuop)
]
