from django.shortcuts import render
from .forms import EnForm
from .models import EnModel
# Create your views here.
def home(request):
    if request.method == "POST":
        data = EnForm(request.POST)
        data.save()
        print(type(data))
        msg = "We will get back to you"
        fm = EnForm()
        return render(request,"home.html",{"fm":fm,"msg":msg})
    fm = EnForm()
    return render(request,"home.html",{"fm":fm})