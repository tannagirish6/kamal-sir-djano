from django.db import models

# Create your models here.
class EnModel(models.Model):
    name  = models.CharField(max_length=40)
    phone = models.IntegerField()
    subject = models.CharField(max_length=40)

    def __str__(self):
        return self.name+" "+self.subject #this shows what to show in DB heading