from django.db import models

# Create your models here.
class WnModel(models.Model):
    name = models.CharField(max_length=40)
    options = models.CharField(max_length=5)

    def __str__(self):
        return self.name