from django import forms

CHOICES = [("job","Job"),("ms","MS"),("mba","MBA")]
class WnForm(forms.Form):
    name = forms.CharField()
    options = forms.CharField(widget=forms.RadioSelect(choices=CHOICES))