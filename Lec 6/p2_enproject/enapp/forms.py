from django import forms
class FbForm(forms.Form):
    name = forms.CharField(max_length=40)
    feedback = forms.CharField(widget=forms.Textarea(attrs={'name':'body', 'rows':3, 'cols':5}))