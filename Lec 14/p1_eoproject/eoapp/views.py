from django.shortcuts import render


def home(request):
    if request.GET.get("num"):
        try:
            num = int(request.GET.get("num"))
            if num % 2 == 0:
                msg = "number "+str(num)+" is even"
            else:
                msg = "number "+str(num)+" is odd"
            return render(request,"home.html",{"msg":msg})
        except ValueError:
            msg = "please enter a valid number"
            return render(request,"home.html",{"msg":msg})
    else:
        return render(request,"home.html")
