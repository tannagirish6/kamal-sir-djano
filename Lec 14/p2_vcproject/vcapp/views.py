from django.shortcuts import render

def home(request):
    if request.GET.get("txt"):
        try:
            v,c = 0,0
            txt = request.GET.get("txt")
            for t in txt:
                if t in "AEIOUaeiou":
                    v += 1
                else:
                    c += 1
            msg = "Vowels",v," Consonents",c
            return render(request,"home.html",{"msg":msg})
        except:
            return render(request,"home.html",{"msg":"enter valid data"})
    else:
        return render(request,"home.html")
