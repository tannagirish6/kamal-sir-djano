from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.
class StudentModel(models.Model):
    rno = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    marks = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)])

    def __str__(self):
        return str(self.rno)+" "+str(self.marks)
